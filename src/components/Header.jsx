import React, { useState } from 'react'

const stateHeader = {
  heading: 'About Us', 
  link1: 'Home', 
  link2: 'About Us'
}

const Header = () => {
  const [state] = useState(stateHeader)
  return (
    <div className="header">
        <div className='header__content'>
          <h1 className="header__content-h1">{state.heading}</h1>
          <div className="header__content-links">
            <a href="">{state.link1}</a>
              <span className='header__content-span'></span>
            <a href="">{state.link2}</a>
          </div>
        </div>
    </div>
  )
}

export default Header