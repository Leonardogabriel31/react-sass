import React from 'react'

const Showcase = () => {
    return (
        <div className="show">
            <div className="container rel">
                <div className="show__content">
                    <h1>
                        start the digital world today
                    </h1>
                    <h1>

                    </h1>
                </div>
                <div className="show__card">
                    <h6 className="show__card-h6">OUR HAPPY CLIENTS</h6>
                    <div className="show__card-h4">What Client's Say?</div>
                    <div className="show__card-text">Lorem, ipsum dolor sit amet consectetur adipisicing 
                    elit. Possimus temporibus optio obcaecati, debitis, provident quae facilis quo cum, sequi et vel. 
                    Ab libero, dolore tempora nisi error ipsam quia rem.</div>
                </div>
            </div>
        </div>
    )
}

export default Showcase